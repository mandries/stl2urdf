import os # to walk through directories, to rename files
import sys

# Libraries
import trimesh # for converting voxel grids to meshes (to import objects into simulators)

# Modules
import tools_urdf_generator
from pathlib import Path # for removing the file extension

def generate_URDF_from_STL(filename, max_side_size_in_meters, storageDirectoryName, verbose=False):

    filename_without_extension = Path(filename).stem
    if (verbose):
        print("Filename: " + filename)
        print("Filename without extension: " + filename_without_extension)

        # Generate a folder to store the images
        print("Generating a folder to save the mesh")
    # Generate a folder with the same name as the input file, without its extension
    currentPathGlobal = os.path.dirname(os.path.abspath(__file__))

    directory = currentPathGlobal + "/" + storageDirectoryName + "/" + filename_without_extension #+ "_urdf"
    if not os.path.exists(directory):
        os.makedirs(directory)

    mesh = trimesh.load(filename)
    # print("Mesh bounds: ")
    # print(mesh.bounds)

    if (verbose):
        print("Mesh extents: ")
        print(mesh.extents)
        print("Max extent: {}".format(max(mesh.extents)))

    scaling_factor = max_side_size_in_meters / max(mesh.extents)
    if (verbose):
        print("Scaling factor: {}".format(scaling_factor))
    mesh.apply_scale(scaling=scaling_factor)

    # mass = 1.00
    mass = mesh.volume # WATER density

    if (verbose):
        print("\n\nMesh volume: {} (used as mass)".format(mesh.volume))
        print("Mass (equal to volume): {0}".format(mass))
        print("Mesh convex hull volume: {}\n\n".format(mesh.convex_hull.volume))
        print("Mesh bounding box volume: {}".format(mesh.bounding_box.volume))

    # print("Merging vertices closer than a pre-set constant...")
    mesh.merge_vertices()
    # print("Removing duplicate faces...")
    mesh.remove_duplicate_faces()
    # print("Making the mesh watertight...")
    trimesh.repair.fill_holes(mesh)
    # print("Fixing inversion and winding...")
    # trimesh.repair.fix_winding(mesh)
    # trimesh.repair.fix_inversion(mesh)
    trimesh.repair.fix_normals(mesh)

    if (verbose):
        print("\n\nMesh volume: {}".format(mesh.volume))
        print("Mesh convex hull volume: {}".format(mesh.convex_hull.volume))
        print("Mesh bounding box volume: {}".format(mesh.bounding_box.volume))

        print("Generating the output (URDF) file...")
    object_model_name = filename_without_extension # "mesh"

    # Computer stable object orientations
    transforms, probs = trimesh.poses.compute_stable_poses(mesh)

    if (verbose):
        print("Transforms: ")
        print(transforms)
        print("Probabilities:")
        print(probs)

        print("Generating the STL mesh file")
    trimesh.exchange.export.export_mesh(
        mesh=mesh,
        file_obj=directory + "/" + filename_without_extension + ".stl",
        file_type="stl"
    )

    # # Roll through all stable object orientations, and generate a corresponding URDF file
    for i in range(0, len(transforms), 1):
        # euler = trimesh.transformations.euler_from_matrix(transforms[i], 'rxyz')
        # print("Euler angles:")
        # print(euler)
        #
        # orientation = str(round(euler[0], 5)) + ' ' + str(round(euler[1], 5)) + ' ' + str(round(euler[2], 5))
        orientation = '0 0 0'

        if (verbose):
            print("Defining the orientation of the output URDF files")
            print("Orientation: " + orientation)
        # orientation = '0 0 0'

        # If required to rotate the meshes (instead of specifying rotation in URDF)
        # mesh_rotated = mesh.apply_transform(rot_matrix)
        mesh_copy = mesh.copy()
        mesh_rotated = mesh_copy.apply_transform(transforms[i])
        # mesh_rotated.show()

        if (verbose):
            print("Generating the STL rotated mesh file")
        trimesh.exchange.export.export_mesh(
            mesh=mesh_rotated,
            file_obj=directory + "/" + filename_without_extension +'_rpy_'+str(i) + ".stl",
            file_type="stl"
            )

        # if (verbose):
        #     print("Computing the center of mass: ")
        # center_of_mass = mesh_rotated.center_mass
        # if (verbose):
        #     print(center_of_mass)

        if (verbose):
            print("Computing moments of inertia: ")
        moments_of_inertia = mesh_rotated.moment_inertia
        if (verbose):
            print(moments_of_inertia)  # inertia tensor in meshlab

        tools_urdf_generator.generate_model(
            directory=directory,
            stl_name=object_model_name +'_rpy_'+str(i),
            urdf_name=object_model_name+'_rpy_'+str(i),
            # center_of_mass=center_of_mass,
            center_of_mass=mesh_rotated.center_mass,
            # inertia_tensor=moments_of_inertia,
            inertia_tensor=mesh_rotated.moment_inertia,
            mass=mass,
            # model_path=directory + "/" + filename_without_extension + ".stl",
            scale_factor = 1.0, #scale_normalisation_factor)
            orientation=orientation)


if __name__ == "__main__":

    print("Usage: ")
    print("python {} <FILEPATH> max_side_size_in_meters".format(sys.argv[0]))
    print("Example:\npython {} <FILEPATH> 1.0".format(sys.argv[0]))


    filename = sys.argv[1]
    max_side_size_in_meters = float(sys.argv[2])

    generate_URDF_from_STL(filename, max_side_size_in_meters)
