import xml.etree.ElementTree as ET
# Reference
# http://sdformat.org/spec?ver=1.9&elem=collision
# https://docs.python.org/3/library/xml.etree.elementtree.html#building-xml-documents

def generate_model(
    directory_source,
    directory_target,
    stl_path_visual,
    stl_path_collision,
    object_model_sim_name,
    sdf_name,
    center_of_mass,
    inertia_tensor,
    mass,
    scale_factor,
    origin,
    orientation):

    # sdf = ET.Element('sdf', {'version': '1.9'})
    sdf = ET.Element('sdf', {'version': '1.7'})

    # model = ET.SubElement(sdf, 'model', {'name': 'objectFromHeapDataset'})
    model = ET.SubElement(sdf, 'model', {'name': object_model_sim_name})
    #
    model_static = ET.SubElement(model, 'static')
    model_static.text = '0'
    #
    model_allowAutoDisable = ET.SubElement(model, 'allow_auto_disable')
    model_allowAutoDisable.text = "1"

    model_pose = ET.SubElement(model, 'pose', {'frame':'/world'})
    model_pose.text = "0 0 0 0 0 0"
    # model_pose.text = str(origin + " " + orientation)

    model_link =  ET.SubElement(model, 'link', {'name':'link0'})
    #
    model_link_selfCollide = ET.SubElement(model_link, 'self_collide')
    model_link_selfCollide.text = "0"
    #
    model_link_kinematic = ET.SubElement(model_link, 'kinematic')
    model_link_kinematic.text = "0"
    #
    model_link_enableWind = ET.SubElement(model_link, 'enable_wind')
    model_link_enableWind.text = "0"
    #
    model_link_gravity = ET.SubElement(model_link, 'gravity')
    model_link_gravity.text = "1"

    model_link_inertial = ET.SubElement(model_link, 'inertial')
    #
    model_link_inertial_pose = ET.SubElement(model_link_inertial, 'pose')
    model_link_inertial_pose.text = "0 0 0 0 -0 0"
    #
    model_link_inertial_mass = ET.SubElement(model_link_inertial, 'mass')
    model_link_inertial_mass.text = str(mass)
    #
    model_link_inertial_inertia = ET.SubElement(model_link_inertial, 'inertia')
    model_link_inertial_inertia_ixx = ET.SubElement(model_link_inertial_inertia, 'ixx')
    model_link_inertial_inertia_ixy = ET.SubElement(model_link_inertial_inertia, 'ixy')
    model_link_inertial_inertia_ixz = ET.SubElement(model_link_inertial_inertia, 'ixz')
    model_link_inertial_inertia_iyy = ET.SubElement(model_link_inertial_inertia, 'iyy')
    model_link_inertial_inertia_iyz = ET.SubElement(model_link_inertial_inertia, 'iyz')
    model_link_inertial_inertia_izz = ET.SubElement(model_link_inertial_inertia, 'izz')
    # Object mass format: scalar
    # Center of Mass format: [x,y,z]
    # Inertia tensor format:
    # | ixx ixy ixz |
    # | ixy iyy iyz |
    # | ixz iyz izz |
    model_link_inertial_inertia_ixx.text = str(round(inertia_tensor[0][0], 10))
    model_link_inertial_inertia_ixy.text = str(round(inertia_tensor[0][1], 10))
    model_link_inertial_inertia_ixz.text = str(round(inertia_tensor[0][2], 10))
    model_link_inertial_inertia_iyy.text = str(round(inertia_tensor[1][1], 10))
    model_link_inertial_inertia_iyz.text = str(round(inertia_tensor[1][2], 10))
    model_link_inertial_inertia_izz.text = str(round(inertia_tensor[2][2], 10))

    # model_link_inertial_frame = ET.SubElement(model_link_inertial, 'frame', {'name': 'link0_inertial_frame'})
    # model_link_inertial_frame_pose = ET.SubElement(model_link_inertial_frame, 'pose', {'frame': 'link0_frame'})
    # model_link_inertial_frame_pose.text = "0 0 0 0 0 0"
    # # This is the pose of the inertial reference frame, relative to the specified reference frame.
    # # The origin of the inertial reference frame needs to be at the center of mass.
    # # The axes of the inertial reference frame do not need to be aligned with the principal axes of the inertia.
    # model_link_inertial_pose = ET.SubElement(model_link_inertial, 'pose', {'frame':'link0_inertial_frame'})
    # # model_link_inertial_pose.text = "0 0 0 0 0 0"
    # model_link_inertial_pose.text = str(center_of_mass[0]) + " " + str(center_of_mass[1]) + " " + str(center_of_mass[2]) + " " + str(orientation[0]) + " " + str(orientation[1]) + " " + str(orientation[2])

    # This is the pose of the inertial reference frame, relative to the specified reference frame.
    # The origin of the inertial reference frame needs to be at the center of gravity.
    # The axes of the inertial reference frame do not need to be aligned with the principal axes of the inertia.

    model_link_visual = ET.SubElement(model_link, 'visual', {'name': 'visual'})
    #
    model_link_visual_pose = ET.SubElement(model_link_visual, 'pose')
    model_link_visual_pose.text = origin + " " + orientation
    #
    model_link_visual_geometry = ET.SubElement(model_link_visual, 'geometry')
    #
    model_link_visual_geometry_mesh = ET.SubElement(model_link_visual_geometry, 'mesh')
    #
    model_link_visual_geometry_mesh_uri = ET.SubElement(model_link_visual_geometry_mesh, 'uri')
    model_link_visual_geometry_mesh_uri.text = stl_path_visual
    #
    model_link_visual_geometry_mesh_scale = ET.SubElement(model_link_visual_geometry_mesh, 'scale')
    model_link_visual_geometry_mesh_scale.text = str(scale_factor) + ' ' + str(scale_factor) + ' ' + str(scale_factor)
    #
    # model_link_visual_material = ET.SubElement(model_link_visual, 'material')
    # model_link_visual_material_script = ET.SubElement(model_link_visual_material, 'script')
    # #
    # model_link_visual_material_script_uri = ET.SubElement(model_link_visual_material_script, 'uri')
    # model_link_visual_material_script_uri.text = "file://media/materials/scripts/gazebo.material"
    # #
    # model_link_visual_material_script_name = ET.SubElement(model_link_visual_material_script, 'name')
    # model_link_visual_material_script_name.text = "Gazebo/White"
    # #
    # model_link_visual_material_shader = ET.SubElement(model_link_visual_material, 'shader', {'type': 'pixel'})
    #
    model_link_visual_transparency = ET.SubElement(model_link_visual, 'transparency')
    model_link_visual_transparency.text = "0"
    #
    model_link_visual_castShadows = ET.SubElement(model_link_visual, 'cast_shadows')
    model_link_visual_castShadows.text = "1"

    # <visual name="visual">
    #     <cast_shadows>1</cast_shadows>
    #     <transparency>0</transparency>
    #     <material>
    #         # <lighting>1</lighting>
    #             #    <ambient>0.15 0.75 0.35 1</ambient>
    #             #    <diffuse>0.1 0.95 0.25 1</diffuse>
    #             #    <specular>0.01 0.01 0.01 1</specular>
    #             #    <emissive>0 0 0 1</emissive>
    #     </material>
    # </visual>

    model_link_collision = ET.SubElement(model_link, 'collision', {'name':'collision'})
    #
    model_link_collision_laserRetro = ET.SubElement(model_link_collision, 'laser_retro')
    model_link_collision_laserRetro.text = "0"
    #
    # Maximum number of contacts allowed between two entities. This value overrides the max_contacts element defined in physics.
    model_link_collision_max_contacts = ET.SubElement(model_link_collision, 'max_contacts')
    model_link_collision_max_contacts.text = "50"
    #
    model_link_collision_pose = ET.SubElement(model_link_collision, 'pose')
    model_link_collision_pose.text = origin + " " + orientation
    #
    model_link_collision_geometry = ET.SubElement(model_link_collision, 'geometry')
    #
    model_link_collision_geometry_mesh = ET.SubElement(model_link_collision_geometry, 'mesh')
    #
    model_link_collision_geometry_mesh_uri = ET.SubElement(model_link_collision_geometry_mesh, 'uri')
    model_link_collision_geometry_mesh_uri.text = stl_path_collision
    #
    model_link_collision_geometry_mesh_scale = ET.SubElement(model_link_collision_geometry_mesh, 'scale')
    model_link_collision_geometry_mesh_scale.text = str(scale_factor) + ' ' + str(scale_factor) + ' ' + str(scale_factor)
    ###
    model_link_collision_surface = ET.SubElement(model_link_collision, 'surface')
    ##
    model_link_collision_surface_bounce = ET.SubElement(model_link_collision_surface, 'bounce')
    #
    model_link_collision_surface_bounce_restitutionCoefficient = ET.SubElement(model_link_collision_surface_bounce, 'restitution_coefficient')
    model_link_collision_surface_bounce_restitutionCoefficient.text = "0" #str(0.25)
    #
    model_link_collision_surface_bounce_threshold = ET.SubElement(model_link_collision_surface_bounce, 'threshold')
    model_link_collision_surface_bounce_threshold.text = "1e+06"
    ##
    ##
    model_link_collision_surface_friction = ET.SubElement(model_link_collision_surface, 'friction')
    #
    model_link_collision_surface_friction_ode = ET.SubElement(model_link_collision_surface_friction, 'ode')
    #
    model_link_collision_surface_friction_ode_mu = ET.SubElement(model_link_collision_surface_friction_ode, 'mu')
    model_link_collision_surface_friction_ode_mu.text = str(1.0)
    #
    model_link_collision_surface_friction_ode_mu2 = ET.SubElement(model_link_collision_surface_friction_ode, 'mu2')
    model_link_collision_surface_friction_ode_mu2.text = str(1.0)
    #
    model_link_collision_surface_friction_ode_fdir1 = ET.SubElement(model_link_collision_surface_friction_ode, 'fdir1')
    model_link_collision_surface_friction_ode_fdir1.text = "0 0 0"
    #
    model_link_collision_surface_friction_ode_slip1 = ET.SubElement(model_link_collision_surface_friction_ode, 'slip1')
    model_link_collision_surface_friction_ode_slip1.text = "0"
    #
    model_link_collision_surface_friction_ode_slip2 = ET.SubElement(model_link_collision_surface_friction_ode, 'slip2')
    model_link_collision_surface_friction_ode_slip2.text = "0"
    ##
    model_link_collision_surface_friction_torsional = ET.SubElement(model_link_collision_surface_friction, 'torsional')
    #
    model_link_collision_surface_friction_torsional_coefficient = ET.SubElement(model_link_collision_surface_friction_torsional, 'coefficient')
    model_link_collision_surface_friction_torsional_coefficient.text = "1"
    #
    model_link_collision_surface_friction_torsional_patchRadius = ET.SubElement(
        model_link_collision_surface_friction_torsional, 'patch_radius')
    model_link_collision_surface_friction_torsional_patchRadius.text = "0"
    #
    model_link_collision_surface_friction_torsional_surfaceRadius = ET.SubElement(
        model_link_collision_surface_friction_torsional, 'surface_radius')
    model_link_collision_surface_friction_torsional_surfaceRadius.text = "0"
    #
    model_link_collision_surface_friction_torsional_usePatchRadius = ET.SubElement(
        model_link_collision_surface_friction_torsional, 'use_patch_radius')
    model_link_collision_surface_friction_torsional_usePatchRadius.text = "1"
    #
    model_link_collision_surface_friction_torsional_ode = ET.SubElement(model_link_collision_surface_friction_torsional, 'ode')
    #
    model_link_collision_surface_friction_torsional_ode_slip = ET.SubElement(model_link_collision_surface_friction_torsional_ode, 'slip')
    model_link_collision_surface_friction_torsional_ode_slip.text = "0"
    ###
    ###
    model_link_collision_surface_contact = ET.SubElement(model_link_collision_surface, 'contact')
    #
    model_link_collision_surface_contact_collideWithoutContact = ET.SubElement(model_link_collision_surface_contact, 'collide_without_contact')
    model_link_collision_surface_contact_collideWithoutContact.text = "0"
    #
    model_link_collision_surface_contact_collideWithoutContactBitmask = ET.SubElement(model_link_collision_surface_contact, 'collide_without_contact_bitmask')
    model_link_collision_surface_contact_collideWithoutContactBitmask.text = "1"
    #
    model_link_collision_surface_contact_collideBitmask = ET.SubElement(model_link_collision_surface_contact, 'collide_bitmask')
    model_link_collision_surface_contact_collideBitmask.text = "1"
    #
    model_link_collision_surface_contact_ode = ET.SubElement(model_link_collision_surface_contact, 'ode')
    #
    model_link_collision_surface_contact_ode_softCfm = ET.SubElement(model_link_collision_surface_contact_ode, 'soft_cfm')
    model_link_collision_surface_contact_ode_softCfm.text = "0"
    #
    model_link_collision_surface_contact_ode_softErp = ET.SubElement(model_link_collision_surface_contact_ode, 'soft_erp')
    model_link_collision_surface_contact_ode_softErp.text = "0.2"
    #
    # Stiffness
    model_link_collision_surface_contact_ode_kp = ET.SubElement(model_link_collision_surface_contact_ode, 'kp')
    model_link_collision_surface_contact_ode_kp.text = "1e+13" # str(325) # steel
    #
    # Damping
    model_link_collision_surface_contact_ode_kd = ET.SubElement(model_link_collision_surface_contact_ode, 'kd')
    model_link_collision_surface_contact_ode_kd.text = "0.01" #"1" # str(0.01)
    #
    model_link_collision_surface_contact_ode_maxVel = ET.SubElement(model_link_collision_surface_contact_ode, 'max_vel')
    model_link_collision_surface_contact_ode_maxVel.text = "0"
    #
    model_link_collision_surface_contact_ode_minDepth = ET.SubElement(model_link_collision_surface_contact_ode, 'min_depth')
    model_link_collision_surface_contact_ode_minDepth.text = "0.003"
    ###
    model_link_collision_surface_contact_bullet = ET.SubElement(model_link_collision_surface_contact, 'bullet')
    #
    model_link_collision_surface_contact_bullet_splitImpulse = ET.SubElement(model_link_collision_surface_contact_bullet, 'split_impulse')
    model_link_collision_surface_contact_bullet_splitImpulse.text = "1"
    #
    model_link_collision_surface_contact_bullet_splitImpulsePenetrationThreshold = ET.SubElement(model_link_collision_surface_contact_bullet, 'split_impulse_penetration_threshold')
    model_link_collision_surface_contact_bullet_splitImpulsePenetrationThreshold.text = "-0.01"
    #
    model_link_collision_surface_contact_bullet_softCfm = ET.SubElement(model_link_collision_surface_contact_bullet, 'soft_cfm')
    model_link_collision_surface_contact_bullet_softCfm.text = "0"
    #
    model_link_collision_surface_contact_bullet_softErp = ET.SubElement(model_link_collision_surface_contact_bullet, 'soft_erp')
    model_link_collision_surface_contact_bullet_softErp.text = "0.2"
    #
    model_link_collision_surface_contact_bullet_kp = ET.SubElement(model_link_collision_surface_contact_bullet, 'kp')
    model_link_collision_surface_contact_bullet_kp.text = "1e+13"
    model_link_collision_surface_contact_bullet_kd = ET.SubElement(model_link_collision_surface_contact_bullet, 'kd')
    model_link_collision_surface_contact_bullet_kd.text = "1"


    # ET.indent(sdf, space="\t", level=0)
    sdf_content = ET.tostring(sdf).decode('utf-8')
    # print(sdf_content)
    sdf_content = "<?xml version='1.0' ?>\n" + sdf_content

    # # Create the file
    f = open(directory_target + "/" + sdf_name + ".sdf", "w")
    # # Write the content to file
    f.write(sdf_content)
    # # Close the file
    f.close()
