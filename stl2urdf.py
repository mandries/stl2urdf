import os # to walk through directories, to rename files
import sys
import numpy

# Libraries
import trimesh # for converting voxel grids to meshes (to import objects into simulators)
# import shutil # for copying files

# Modules
import tools_urdf_generator
import tools_sdf_generator
from pathlib import Path # for removing the file extension

def generate_URDF_from_STL(
    filename, # Path to the file to convert
    max_side_size_in_meters,
    storageDirectoryName, # target directory name (not path)
    verbose=False,
    MAX_STABLE_POSES=4):

    # Name the folder containing the STL+SDF+URDF like the OBJ/PLY file
    # filename_without_extension = Path(filename).stem

    # Name the folder containing the STL+SDF+URDF like the folder containing the PLY file (used for YCB dataset)
    # print("Filename split: ");
    # print(filename.split("/"));
    # print(filename.split("/")[-1]);
    # filename_without_extension = filename.split("/")[2]
    filename_without_extension = filename.split("/")[-1];
    # print("filename without extension: " + filename_without_extension)

    directory_source = os.path.dirname(os.path.abspath(filename));

    if (verbose):
        print("Filename: " + filename)
        print("Filename without extension: " + filename_without_extension)

        # Generate a folder to store the images
        print("Generating a folder to save the mesh")

    # Generate a folder with the same name as the input file, without its extension
    currentPathGlobal = os.path.dirname(os.path.abspath(__file__))
    directory_target = currentPathGlobal + "/" + storageDirectoryName + "/" + filename_without_extension
    if not os.path.exists(directory_target):
        os.makedirs(directory_target)

    mesh = trimesh.load(filename, process=True)
    if (verbose): print("Merging vertices closer than a pre-set constant...")
    mesh.merge_vertices()
    if (verbose): print("Removing duplicate faces...")
    mesh.remove_duplicate_faces()
    if (verbose): print("Making the mesh watertight...")
    # mesh.process()
    trimesh.repair.fill_holes(mesh)
    # print("Fixing inversion and winding...")
    # trimesh.repair.fix_winding(mesh)
    # trimesh.repair.fix_inversion(mesh)
    trimesh.repair.fix_normals(mesh)

    # print("Mesh bounds: ")
    # print(mesh.bounds)
    if (verbose):
        print("Mesh extents: ")
        print(mesh.extents)
        print("Max extent: {}".format(max(mesh.extents)))

    # Scale the objet inside the SDF file, not inside the mesh file itself
    scaling_factor = round(max_side_size_in_meters / max(mesh.extents),5)
    if (verbose): print("Scaling factor: {}".format(scaling_factor))
    mesh.apply_scale(scaling=scaling_factor)
    scaling_factor=1.0

    # mass = 0.00
    mesh.density = 1000 # water # kg per cubic meter
    # mass = mesh.mass
    # mass = mesh.volume * mesh.density

    if (verbose):
        print("\n\nMesh volume: {} (used as mass)".format(mesh.volume))
        print("Mass (equal to volume): {0}".format(mesh.mass))
        print("Mesh convex hull volume: {}\n\n".format(mesh.convex_hull.volume))
        print("Mesh bounding box volume: {}".format(mesh.bounding_box.volume))

    # # Set the mesh origin (0 0 0) at the object center of mass,
    # # mesh.Translate(-Centroid or -CenterOfMass)
    # transformation_matrix = trimesh.transformations.translation_matrix(-mesh.center_mass)
    # mesh = mesh.apply_transform(transformation_matrix)
    # # print("Translation matrix: ")
    # # print(transformation_matrix)

    transformation_matrix = mesh.principal_inertia_transform
    mesh.apply_transform(transformation_matrix)
    # print("Principal inertia transform:")
    # print(transformation_matrix)
    # Translate the mesh so that all vertex vertices are at positive coordinates.
    # mesh.rezero()

    # print("Principal inertia vectors:")
    # print(mesh.principal_inertia_vectors)
    # print("Moment inertia:")
    # print(mesh.moment_inertia)

    if (verbose):
        print("\n\nMesh volume: {}".format(mesh.volume))
        print("Mesh convex hull volume: {}".format(mesh.convex_hull.volume))
        print("Mesh bounding box volume: {}".format(mesh.bounding_box.volume))

        print("Computing the center of mass: ")
        print(mesh.center_mass)

        print("Computing moments of inertia: ")
        print(mesh.moment_inertia)  # inertia tensor in meshlab
        # Return the moment of inertia matrix of the current mesh.
        # Warning: If mesh isn’t watertight this is garbage.
        print("Watertight mesh? {}".format(mesh.is_watertight))


    # If required to rotate the meshes (instead of specifying rotation in URDF)
    # mesh_rotated = mesh.apply_transform(rot_matrix)
    # mesh_rotated = mesh.apply_transform(transforms[i])

    if (verbose): print("Generating the output (URDF) file...")
    object_model_name = filename_without_extension # "mesh"

    # Computer stable object orientations
    transforms, probs = trimesh.poses.compute_stable_poses(mesh)

    if (verbose):
        print("Transforms: ")
        print(transforms)
        print("Probabilities:")
        print(probs)

        print("Generating the STL mesh file")

    # print("Mesh in STL format: " + trimesh.exchange.stl.export_stl_ascii(mesh))
    # # Create the file
    f = open(directory_target + "/" + filename_without_extension + ".stl", "w")
    # # Write the content to file
    f.write(trimesh.exchange.stl.export_stl_ascii(mesh))
    # # Close the file
    f.close()

    # Option: Save stl in Byte64 format (not human-readable)
    # trimesh.exchange.export.export_mesh(
    #     mesh=mesh,
    #     file_obj=directory_target + "/" + filename_without_extension + ".stl",
    #     file_type="stl"
    # )

    # # Roll through all stable object orientations, and generate a corresponding URDF file
    for i in range(0, min(len(transforms), MAX_STABLE_POSES), 1):
        euler = trimesh.transformations.euler_from_matrix(transforms[i], 'rxyz')
        if (verbose):
            print("Euler angles:")
            print(euler)

        orientation = str(euler[0]) + ' ' + str(euler[1]) + ' ' + str(euler[2])
        # orientation = '0 0 0'
        origin='0 0 0'
        # origin = str(-mesh.center_mass).replace('[','').replace(']','')

        if (verbose):
            print("Defining the orientation of the output URDF files")
            print("Orientation: " + orientation)

        tools_urdf_generator.generate_model(
            directory=directory_target,
            stl_name='/root/shapes/' + object_model_name,
            urdf_name=object_model_name+'_rpy_'+str(i),
            center_of_mass=mesh.center_mass,
            inertia_tensor=mesh.moment_inertia,
            mass=mesh.mass,
            scale_factor = scaling_factor, #1.0, #scale_normalisation_factor)
            origin=origin,
            orientation=orientation)

        tools_sdf_generator.generate_model(
            directory_source=directory_source,
            directory_target=directory_target,
            stl_path_visual='/root/shapes/'+object_model_name+'.stl',
            stl_path_collision='/root/shapes/'+object_model_name+'.stl',
            object_model_sim_name=object_model_name,
            sdf_name=object_model_name+'_rpy_'+str(i),
            center_of_mass=numpy.around(mesh.center_mass,5),
            inertia_tensor=mesh.moment_inertia,
            mass=mesh.mass,
            scale_factor = scaling_factor, #1.0, #scale_normalisation_factor)
            origin=origin,
            orientation=orientation)

    print("Done!")

if __name__ == "__main__":

    print("Usage: ")
    print("python {} <FILEPATH> max_side_size_in_meters".format(sys.argv[0]))
    print("Example:\npython {} <FILEPATH> 1.0".format(sys.argv[0]))

    filename = sys.argv[1]
    max_side_size_in_meters = float(sys.argv[2])

    generate_URDF_from_STL(filename, max_side_size_in_meters, ".")
