import os # to walk through directories, to rename files
import sys

# Libraries
import trimesh # for converting voxel grids to meshes (to import objects into simulators)

# Modules
import tools_urdf_generator
import stl2urdf
# import stl2urdf_ycb_mihai as stl2urdf
# import stl2urdf_multiple_rpy_mesh as stl2urdf
from datetime import datetime

def process_all_stl_from_directory(
        dataset_path,
        max_side_size_in_meters):
  print("Going through the directory of the dataset...")
  dataset_of_filenames = []
  # Walk through the directory, and pick the files
  for root, dirs, files in os.walk(dataset_path, topdown=True):  # directory
    # for directory in dirs:
      # print('\nFound: ' + directory + '/')
    for filename in files:
        # print('\nFound: ' + filename)
        if filename.endswith('.obj'):
        # if filename.endswith('nontextured.ply'):
        # if filename.endswith('nontextured.stl'):
        # if filename.endswith('textured.obj'):
            # print(filename)
            global_filename = os.path.join(root, filename)
            # stl2urdf.generate_URDF_from_STL(global_filename, max_side_size_in_meters)
            dataset_of_filenames.append(global_filename)

  print("Files to process: " + str(len(dataset_of_filenames)))
  if (len(dataset_of_filenames)>0):
      now = datetime.now()
      date_time = now.strftime("gen_%Y-%m-%d %Hh %Mm %Ss")
      print("date and time:",date_time)
      storageDirectoryName = date_time # it will contain the results

  for mesh_filename in dataset_of_filenames:
      print(mesh_filename)
      stl2urdf.generate_URDF_from_STL(mesh_filename, max_side_size_in_meters, storageDirectoryName)

if __name__ == "__main__":

    print("Hello! Here are the file formats that I can process:")
    print(trimesh.exchange.load.available_formats())

    dataset_path = sys.argv[1]
    print("Dataset path: " + dataset_path)
    max_side_size_in_meters = float(sys.argv[2])
    print("Max side size in meters: " + str(max_side_size_in_meters))
    process_all_stl_from_directory(dataset_path, max_side_size_in_meters)
