# stl2sdf

Converter of meshes from STL/OBJ to URDF format.

Takes as input a mesh file (STL/OBJ) and generates the corresponding corresponding .URDF file for importing the model into Gazebo.
It uses the [TriMesh](https://github.com/mikedh/trimesh) library.  
It also allows to resize a mesh.

```bash
# Usage
python stl2urdf.py <FILENAME> <DESIRED_DIMENSION_FOR_SIDE_IN_METERS>

# Example:
python stl2urdf.py mesh.stl 0.2

# To convert an entire dataset of objects, use the following command:
python stl2urdf_dataset.py <path_to_dataset> <resize_to_size_in_meters>

# Example:
python3.9 stl2urdf_dataset.py "HEAP_object_set/2020.06.12_02h00 dataset/" 0.07
```

### Example usage with Gazebo:

```bash
# Generate the SDF description file for your mesh (without re-sizing it)
# python stl2urdf.py mesh.stl resize_to_size_in_meters
python stl2urdf.py mesh.stl 0.07
# python stl2urdf.py mesh.stl 1.0

# If you wan to import the generated models into gazebo, do the following:

# Launch Gazebo:
roslaunch gazebo_ros empty_world.launch gui:=true

# Spawn the object inside Gazebo
rosrun gazebo_ros spawn_model -sdf -file /path/to/sdf/file/mesh.sdf -model myModelName
```

### (optional) Trimesh installation (dependency)
To install Trimesh, open a terminal and write:
```bash
pip install --user trimesh
```
