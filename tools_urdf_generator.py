import xml.etree.ElementTree as ET
# Reference: http://wiki.ros.org/urdf/XML/link
# Origin = Centroid? (not 000, not centre of mass)

def generate_model(
    directory,
    stl_name,
    urdf_name,
    center_of_mass,
    inertia_tensor,
    mass,
    scale_factor,
    origin,
    orientation):

    robot = ET.Element('robot', {'name': 'Shape'}) # stl_name
    link1 = ET.SubElement(robot, 'link', {'name': 'link1'})

    # print(str(center_of_mass).replace('[','').replace(']',''))
    # center_of_mass_str = str(center_of_mass).replace('[','').replace(']','')

    # Collision tag
    collision = ET.SubElement(link1, 'collision')
    origin_collision= ET.SubElement(collision, 'origin', {
        'xyz':origin,
        # 'xyz':'0 0 0',
        # 'xyz':center_of_mass_str,
        'rpy':orientation})
    geometry_collision = ET.SubElement(collision, 'geometry')
    # Global path:
    # mesh = ET.SubElement(geometry, 'mesh', {'filename':model_path})
    # Relative path:
    mesh_collision = ET.SubElement(geometry_collision, 'mesh', {
        'filename':stl_name + '.stl',
        'scale': str(scale_factor) + ' ' + str(scale_factor) + ' ' + str(scale_factor)
    })

    # Visual tag
    visual = ET.SubElement(link1, 'visual')
    visual_origin= ET.SubElement(visual, 'origin', {
        # 'xyz':center_of_mass_str,
        # 'xyz':'0 0 0',
        'xyz':origin,
        'rpy':orientation
        })
    visual_geometry = ET.SubElement(visual, 'geometry')
    mesh_visual = ET.SubElement(visual_geometry, 'mesh',{
        'filename':stl_name + '.stl',
        'scale': str(scale_factor) + ' ' + str(scale_factor) + ' ' + str(scale_factor)
        #'1 1 1'
        })
    visual_material = ET.SubElement(visual, 'material', {'name': 'green'})
    visual_material_color = ET.SubElement(visual_material, 'color', {'rgba': '0 1 0 0'}) # green colored object

    # Inertial tag
    inertial = ET.SubElement(link1, 'inertial')
    origin_inertial = ET.SubElement(inertial, 'origin', {
        # 'xyz':center_of_mass_str,
        # 'xyz':'0 0 0',
        'xyz':origin,
        'rpy':'0 0 0'})
        # 'rpy':orientation}) #
    # WARNING: Do not use rounding for the mass or the inertia matrix values!
    # It will make irrealistic the physical behaviour of the object.
    # mass = round(mass, 5)
    ixx = round(inertia_tensor[0][0],10)
    ixy = round(inertia_tensor[0][1],10)
    ixz = round(inertia_tensor[0][2],10)
    iyy = round(inertia_tensor[1][1],10)
    iyz = round(inertia_tensor[1][2],10)
    izz = round(inertia_tensor[2][2],10)
    #
    inertia = ET.SubElement(inertial, 'inertia', {
      'ixx':str(ixx),
      'ixy':str(ixy),
      'ixz':str(ixz),
      'iyy':str(iyy),
      'iyz':str(iyz),
      'izz':str(izz)
    })
    #
    mass = ET.SubElement(inertial, 'mass', {'value':str(mass)})

    # Convert the ElementTree to a string
    ### Indent the string (works only for Python 3.9+)
    ET.indent(robot, space="\t", level=0)
    urdf_content = ET.tostring(robot).decode('utf-8')
    # print(str(ET.tostring(robot)))
    # print(urdf_content)

    # Create the file
    f = open(directory + "/" + urdf_name + ".urdf", "w")
    # Write the content to file
    f.write(urdf_content)
    # Close the file
    f.close()
